#!/bin/bash
# Author: Maciej Procyk

SEPARATOR="---------------------------------------------------------------------------------------"

if [[ $# -lt 2 ]]
then
    echo "Usage: ./test.sh <application> <tests-directory>"
    exit 1
fi

if [[ ! -f "$1" ]]
then
    echo -e "File $1 does not exist\nLeaving testing script..."
    exit 1
fi

if [[ ! -d "$2" ]]
then
    echo -e "Directory $2 does not exist\nLeaving testing script..."
    exit 1
fi


error=0
testsNum=0

SAVE_IFS=$IFS
IFS=$(echo -en "\n")

for f in $2/*.in
do
    # delete double slashes from file path
    f=$(echo "$f" | sed s#//*#/#g)

    $1 <${f} >${f%in}tempout 2>${f%in}temperr

    if [[ -f "${f%in}out" ]]
	then
		echo "File ${f%in}out test:"
		if diff --text ${f%in}tempout ${f%in}out >${f%in}logout 2>&1
		then
			echo -e "\e[92mGenerated standard output file is correct\e[39m"
			rm -rf "${f%in}logout"
		else
			echo -e "\e[91m---> There is a difference in generated standard output file <---\e[39m"
			error=$((error+1))
		fi
		testsNum=$((testsNum+1))
	fi
	
	if [[ -f "${f%in}err" ]]
	then
		echo "File ${f%in}err test:"
		if diff --text ${f%in}temperr ${f%in}err >${f%in}logerr 2>&1
		then
			echo -e "\e[92mGenerated error output file is correct\e[39m"
			rm -rf "${f%in}logerr"
		else
			echo -e "\e[91m---> There is a difference in generated error output file <---\e[39m"
			error=$((error+1))
		fi
		testsNum=$((testsNum+1))
	fi

    # remove temporary test files from tests directory
     rm -f ${f%in}tempout ${f%in}temperr

    echo ${SEPARATOR}
done

IFS=${SAVE_IFS}

# print the stats from tests
echo -e "\e[1m\nTESTS RESULTS:\e[0m"
echo -e "\t\e[92m$((testsNum-error)) correctly\e[39m passed tests of input/output files tests"
echo -e "\t\e[91m$((error)) errors\e[39m found in input/output files tests"