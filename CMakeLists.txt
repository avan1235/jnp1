cmake_minimum_required(VERSION 3.10)
project(jnp_kasa_biletowa)

set(CMAKE_CXX_STANDARD 17)

add_executable(jnp_kasa_biletowa main.cpp)