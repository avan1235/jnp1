#include <iostream>
#include <vector>
#include <utility>
#include <variant>
#include <optional>
#include <map>
#include <limits>
#include <sstream>
#include <algorithm>
#include <string>
#include <exception>
#include <set>
#include <deque>

using Time = std::pair<int, int>;
using RoutePart = std::pair<Time, std::string>;
using Route = std::vector<RoutePart>;
/**
 * Map from route number to route properties
 */
using Schedule = std::map<int, Route>;
using Price = std::pair<int, int>;

static Price operator+(const Price &p1, const Price &p2)
{
    int rest = (p1.second + p2.second) % 100;
    int add = (p1.second + p2.second) / 100;
    return {p1.first + p2.first + add, rest};
}

using Ticket = std::pair<Price, int>;

/**
 * Map from ticket name to ticket properties
 */
using Tickets = std::map<std::string, Ticket>;
using TicketsEntry = std::pair<std::string, Ticket>;
using TicketsAnswer = std::vector<std::string>;

using Question = std::pair<std::vector<std::string>, std::vector<int>>;

/**
 * As an answer the best collection is returned or the string
 * containing the stop name with TICKET_WAIT_ANSWER_PREFIX or if no
 * possible way to buy ticket then just the string of TICKET_NEGATIVE_ANSWER
 */
using Answer = std::variant<TicketsAnswer, std::string>;

static const std::string TICKET_QUESTION_PREFIX = "?";
static const std::string TICKET_POSITIVE_ANSWER_PREFIX = "! ";
static const std::string TICKET_WAIT_ANSWER_PREFIX = ":-( ";
static const std::string TICKET_NEGATIVE_ANSWER = ":-|";
static const std::string TICKETS_ANSWER_SEPARATOR = "; ";
static const std::string ERROR_LINE_PREFIX = "Error in line ";
static const std::string ERROR_LINE_SEPARATOR = ": ";
static const std::string EMPTY_STRING = "";

static const char TIME_SEPARATOR = ':';
static const char PRICE_DELIMITER = '.';
static const char EXTRA_TICKET_NAME_CHAR = ' ';
static const std::set<char> EXTRA_STOP_NAME_CHARS = {'_', '^'};
static const char WORD_SEPARATOR = ' ';
static const char ZERO_CHAR = '0';

static const int MAX_HOUR_DIGITS = 2;
static const int MINUTE_DIGITS = 2;
static const int MAX_MINUTES = 59;
static const int MIN_PRICE_LENGTH = 4;
static const int PRICE_PRECISION = 2;
static const int LINES_COUNTER_MIN = 1;
static const int MINUTES_IN_HOUR = 60;
static const int MAX_TICKETS = 3;
static const int MIN_TICKET_QUESTION_ARGUMENTS = 4;
static const int MIN_NEW_ROUTE_ARGUMENTS = 3;
static const int TIME_PARTS = 2;
static const int INF = std::numeric_limits<int>::max();

static const Price MIN_PRICE = {0, 0};
static const Time ZERO_TIME = {0, 0};
static const Time START_TIME = {5, 55};
static const Time END_TIME = {21, 21};


enum class ProgramOption {
    ADD_ROUTE,
    ADD_TICKET,
    ASK_FOR_TICKET,
    EMPTY_LINE,
    ERROR_INPUT
};

/**
 * Detect the input type from input string and return predefined
 * input code for specific parsing and extracting data of input string
 * @param input_line to be used to detect the input of
 * @return specified code for input string action:
 * ADD_ROUTE        - add new route to program data
 * ADD_TICKET       - add new ticket to program data
 * ASK_FOR_TICKET   - search for the best fit for tickets for specified route
 * ERROR_INPUT      - not specified action by data
 */
static ProgramOption detect_input(const std::string &input_line);

/**
 * @brief Extract ticket from input line
 * Extract ticket from input line by analyzing the line and if it is possible
 * then create an instance of new ticket. When errors in input then has the
 * std::nullopt
 * @param input_line to be parsed
 * @return the optional value of new ticket parsed from line with  its name
 * as the first element of the returned pair
 */
static std::optional<std::pair<std::string, Ticket>> get_new_ticket(const std::string &input_line);

/**
 * @brief Extract route from input line
 * Extract route from input line by analyzing the line and if it is possible
 * then create an instance of new route. When errors in input then has the
 * std::nullopt
 * @param input_line to be parsed
 * @return the optional value of new route parsed from line with its number as
 * the first element of part to place the route in a good place of schedule
 */
static std::optional<std::pair<int, Route>> get_new_route(const std::string &input_line);

/**
 * @brief Extract question from input line
 * Extract question from input line by analyzing the line and if it is possible
 * then create an instance of new question. When errors in input then has the
 * std::nullopt
 * @param input_line to be parsed
 * @return the optional value of new question parsed from line
 */
static std::optional<Question> ask_for_tickets(const std::string &input_line);

/**
 * @brief Solve the tickets fitting problem and give an Answer
 * Based on tickets data and the possible routes data tries to find the
 * cheapest set of at most three tickets that will satisfy the given
 * question which is a collection of names of stops and the collection
 * of numbers of routes (number of stops = number of routes parts + 1)
 * @param question the specified question by stops and routes
 * @param tickets which can be used to travel by
 * @param schedule which defines the times of arrivals etc.
 * @return best answer for specified question
 */
static Answer find_best_route(const Question &question, Tickets &tickets, Schedule &schedule);

/**
 * Print the answer to standard output based on the content of answer
 * @param answer when have ticket prints tickets in format
 * ! nazwa_biletu; nazwa_biletu_2; ...; nazwa_biletu_n
 * otherwise prints the string included inside the answer
 */
static void print_answer(const Answer &answer);

/**
 * Prints line to standard error and gives the information
 * in which line in program execution the error occured
 * @param input_line to be printed
 * @param line_number in which the error occured
 */
static void print_error(const std::string &input_line, const size_t &line_number);

/**
 * Check if specified in question route exists in schedule and whether it
 * is possible to travel in given way even with need of waiting on
 * samoe stops in route
 * @param schedule in which routes are specified
 * @param question tah describes the our route to be checked
 * @return true when route exists in schedule otherwise false
 */
static bool is_question_route_possible(const Schedule &schedule, const Question &question);

/**
 * Process the input line into current structure by anylyze of input line
 * and extracting data from it and then trying to make some changes in schedule or
 * tickets if possible
 * @param option detected option to be run by this function
 * @param tickets current set of tickets that can be changed
 * @param schedule current schedule that can be changed
 * @param input_line current input line to extract data from
 * @param total_tickets current number of tickets that can be changed
 * @param line_number current line number of input
 */
static void process_input(const ProgramOption &option, Tickets& tickets, Schedule& schedule,
        const std::string &input_line, size_t &total_tickets, const size_t &line_number);

int main() {
    size_t total_tickets = 0;
    size_t line_number = LINES_COUNTER_MIN;

    Tickets tickets;
    Schedule schedule;

    std::string input_line;
    ProgramOption option;

    while (std::getline(std::cin, input_line)) {
        option = detect_input(input_line);
        process_input(option, tickets, schedule, input_line, total_tickets, line_number);
        line_number++;
    }

    std::cout << total_tickets << std::endl;
    return 0;
}

static void process_input(const ProgramOption &option, Tickets& tickets, Schedule& schedule,
                          const std::string &input_line, size_t &total_tickets, const size_t &line_number)
{
    try {
        switch (option) {
            case ProgramOption::ADD_ROUTE: {
                auto route_result = get_new_route(input_line).value();
                if (!schedule.insert(
                        {route_result.first, route_result.second}).second) {
                    // error because key route number already exists in schedule
                    print_error(input_line, line_number);
                }
            } break;
            case ProgramOption::ADD_TICKET: {
                auto ticket_result = get_new_ticket(input_line).value();
                if (!tickets.insert({ticket_result.first,
                                     ticket_result.second}).second) {
                    // error because key ticket name already exists in tickets
                    print_error(input_line, line_number);
                }
            } break;
            case ProgramOption::ASK_FOR_TICKET: {
                if (!is_question_route_possible(
                        schedule, ask_for_tickets(input_line).value())) {
                    // error because given route is impossible with given schedule
                    print_error(input_line, line_number);
                } else {
                    auto answer = find_best_route(
                            ask_for_tickets(input_line).value(), tickets, schedule);
                    if(auto tickets_answer = std::get_if<TicketsAnswer>(&answer))
                        total_tickets += tickets_answer->size();
                    print_answer(answer);
                }
            } break;
            case ProgramOption::ERROR_INPUT: {
                print_error(input_line, line_number);
            } break;
            case ProgramOption::EMPTY_LINE: {
                // do not do anything on empty line
            } break;
        }
    } catch (const std::bad_optional_access &) { // bad input value read
        print_error(input_line, line_number);
    }
}

static bool only_digits_in(const std::string &str)
{
    return std::all_of(str.begin(), str.end(), ::isdigit);
}

static bool is_valid_ticket_name_char(const char &c)
{
    return std::isalpha(c) || c == EXTRA_TICKET_NAME_CHAR;
}

static bool is_valid_stop_name_char(const char &c)
{
    return std::isalpha(c) || EXTRA_STOP_NAME_CHARS.count(c);
}

static bool is_valid_stop_name(const std::string &str)
{
    return std::all_of(str.begin(), str.end(), is_valid_stop_name_char);
}

static bool is_valid_price_char(const char &c)
{
    return std::isdigit(c) || c == PRICE_DELIMITER;
}

static bool starts_with(const std::string &input, const std::string &with)
{
    return !input.rfind(with, 0);
}

static bool has_leading_zeros(const std::string &str)
{
    return only_digits_in(str) && str.length() > 1 && str.front() == ZERO_CHAR;
}

static bool is_price(const std::string &str)
{
    if (str.length() < MIN_PRICE_LENGTH)
        return false;
    if (str.front() == ZERO_CHAR && str.length() != MIN_PRICE_LENGTH)
        return false;
    if (str.at(str.length() - PRICE_PRECISION - 1) != PRICE_DELIMITER)
        return false;
    return std::all_of(str.begin(), str.end(), is_valid_price_char);
}

static std::optional<std::string> extract_ticket_name(const std::string &str)
{
    size_t first_after_name = 0;
    while (first_after_name < str.size() && !std::isdigit(str[first_after_name])) {
        if (!is_valid_ticket_name_char(str[first_after_name]))
            return std::nullopt;
        first_after_name++;
    }

    if (first_after_name == str.size())
        return std::nullopt;

    return str.substr(0, first_after_name - 1);
}

static bool has_valid_word_separation(const std::string &str)
{
    if (str.front() == WORD_SEPARATOR || str.back() == WORD_SEPARATOR)
        return false;

    size_t counter = 0;
    for (const char &c : str) {
        if (c == WORD_SEPARATOR) counter++; else counter = 0;
        if (counter > 1) return false;
    }
    return true;
}

static std::deque<std::string> tokenize(const std::string &input_line,
                                        const char &delimiter = WORD_SEPARATOR)
{
    std::deque<std::string> tokens;
    std::string token;
    std::stringstream sstream(input_line);

    while (std::getline(sstream, token, delimiter))
        tokens.push_back(token);

    return tokens;
}


static std::optional<Time> string_to_time(const std::string &str)
{
    if (str.find(TIME_SEPARATOR) == std::string::npos)
        return std::nullopt;

    std::deque<std::string> tokens = tokenize(str, TIME_SEPARATOR);
    if (tokens.size() != TIME_PARTS) return std::nullopt;

    if (!only_digits_in(tokens.front()) || !only_digits_in(tokens.back()))
        return std::nullopt;

    std::string hour_string;
    if (has_leading_zeros(tokens.front())
        || tokens.back().length() != MINUTE_DIGITS || tokens.front().length() > MAX_HOUR_DIGITS)
        return std::nullopt;

    Time time;
    time.first = std::stoi(tokens.front()), time.second = std::stoi(tokens.back());

    if (!(time >= START_TIME && time <= END_TIME) || time.second > MAX_MINUTES)
        return std::nullopt;
    return time;
}

static Price valid_string_to_price(const std::string &str)
{
    std::deque<std::string> tokens = tokenize(str, PRICE_DELIMITER);
    Price result_price;
    result_price.first = std::stoi(tokens.front());
    result_price.second = std::stoi(tokens.back());
    return result_price;
}

ProgramOption detect_input(const std::string &input_line)
{
    if (input_line.empty())
        return ProgramOption::EMPTY_LINE;

    if (starts_with(input_line, TICKET_QUESTION_PREFIX))
        return ProgramOption::ASK_FOR_TICKET;
    else {
        std::istringstream sstream(input_line);
        std::string word;
        sstream >> word;

        if (!word.empty()) {
            if (only_digits_in(word) && starts_with(input_line, word))
                return ProgramOption::ADD_ROUTE;
            else
                return ProgramOption::ADD_TICKET;
        }
    }
    return ProgramOption::ERROR_INPUT;
}

static std::optional<std::pair<std::string, Ticket>> get_new_ticket(const std::string &input_line)
{
    auto ticket_name = extract_ticket_name(input_line).value_or(EMPTY_STRING);
    // +1 to not include WORD_SEPARATOR in string suffix
    if (ticket_name.empty() || input_line.length() <= ticket_name.length() + 1
        || input_line.at(ticket_name.length()) != WORD_SEPARATOR)
        return std::nullopt;

    std::string input_suffix = input_line.substr(ticket_name.length() + 1);
    if (!has_valid_word_separation(input_suffix))
        return std::nullopt;

    std::deque<std::string> tokens = tokenize(input_suffix);
    std::string word;

    if (tokens.empty()) return std::nullopt;
    word = tokens.front();
    tokens.pop_front();
    if (!is_price(word)) return std::nullopt;

    Price ticket_price = valid_string_to_price(word);

    if (tokens.empty()) return std::nullopt;
    word = tokens.front();
    tokens.pop_front();
    if(!only_digits_in(word) || has_leading_zeros(word)
       || (word.size() == 1 && word[0] == ZERO_CHAR) || !tokens.empty())
        return std::nullopt;

    Ticket ticket = {ticket_price, stoi(word)};
    return std::make_pair(ticket_name, ticket);
}

static std::optional<std::pair<int, Route>> get_new_route(const std::string &input_line)
{
    if (!has_valid_word_separation(input_line))
        return std::nullopt;

    std::set<std::string> stop_names;
    Time prev_time = ZERO_TIME;
    std::string word_time, word_name;

    std::deque<std::string> tokens = tokenize(input_line);

    if (tokens.empty() || tokens.size() % 2 != 1 || tokens.size() < MIN_NEW_ROUTE_ARGUMENTS)
        return std::nullopt;
    if (!only_digits_in(tokens.front())) return std::nullopt;
    int route_number = std::stoi(tokens.front());
    tokens.pop_front();

    Route route;
    while (!tokens.empty()) {
        word_time = tokens.front();
        tokens.pop_front();
        if (tokens.empty()) return std::nullopt;
        word_name = tokens.front();
        tokens.pop_front();

        if (word_time.empty() || word_name.empty()) return std::nullopt;
        auto time = string_to_time(word_time);
        if (time.value_or(ZERO_TIME) <= prev_time) return std::nullopt;
        if (!is_valid_stop_name(word_name) || !stop_names.insert(word_name).second)
            return std::nullopt;
        route.push_back({time.value(), word_name});
        prev_time = time.value();
    }
    if (route.empty())
        return std::nullopt;
    return std::make_pair(route_number, route);
}

static std::optional<Question> ask_for_tickets(const std::string &input_line)
{
    if (!has_valid_word_separation(input_line))
        return std::nullopt;

    Question question;
    std::vector<std::string> stops;
    std::vector<int> route_numbers;
    bool number_last = true;
    std::deque<std::string> tokens = tokenize(input_line);
    if (tokens.size() < MIN_TICKET_QUESTION_ARGUMENTS)
        return std::nullopt;

    tokens.pop_front();
    while (!tokens.empty()) {
        if (is_valid_stop_name(tokens.front()) && number_last) {
            stops.push_back(tokens.front());
        }
        else if (only_digits_in(tokens.front()) && !number_last) {
            route_numbers.push_back(stoi(tokens.front()));
        }
        else return std::nullopt;
        number_last = !number_last;
        tokens.pop_front();
    }

    if (stops.size() - route_numbers.size() != 1)
        return std::nullopt;

    return std::make_pair(stops, route_numbers);
}

static std::optional<Time> time_at_stop_in(const Schedule &schedule, const std::string &stop_name, int route_number)
{
    try {
        Route route = schedule.at(route_number);
        for (auto const &route_part: route)
            if (stop_name == route_part.second)
                return route_part.first;
    } catch (const std::out_of_range &) { }
    return std::nullopt;
}

static void add_ticket_cost(Price &curr_price, int &curr_minutes, Ticket &ticket_to_add)
{
    curr_price = curr_price + ticket_to_add.first;
    curr_minutes = curr_minutes + ticket_to_add.second;
}

static int minutes_between(const Time &start_time, const Time &end_time)
{
    return (end_time.first - start_time.first) * MINUTES_IN_HOUR
           + end_time.second - start_time.second + 1;
}

static bool is_question_route_possible(const Schedule &schedule, const Question &question)
{
    auto stops = question.first;
    auto routes = question.second;

    try {
        Time curr_time = time_at_stop_in(schedule, stops.front(), routes.front()).value();

        if (((time_at_stop_in(schedule, stops.front(), routes.front()).value()
              >= time_at_stop_in(schedule, stops.back(), routes.back()).value())
             || stops.back() == stops.front()) && stops.size() == 2)
            return false;

        for (size_t i = 1; i < stops.size() - 1; ++i) {
            auto before_time = time_at_stop_in(schedule, stops[i], routes[i - 1]);
            auto after_time = time_at_stop_in(schedule, stops[i], routes[i]);

            // if a stop is repeated the route is not impossible
            if(stops[i] == stops[i-1] || after_time.value() < before_time.value()
               || before_time.value() <= curr_time || after_time.value() <= curr_time)
                return false;

            curr_time = after_time.value();
        }
        auto time_at_last_stop = time_at_stop_in(schedule, stops.back(), routes.back());
        return !(time_at_last_stop.value() <= curr_time);

    } catch (const std::bad_optional_access &) {
        return false;
    }
}

static std::optional<std::string> get_waiting_stop_name(Schedule &schedule, const Question &question)
{
    auto stops = question.first;
    auto routes = question.second;

    for (size_t i = 1; i < stops.size() - 1; ++i) {
        auto ith_stop_fst_route = time_at_stop_in(schedule, stops[i], routes[i - 1]);
        auto ith_stop_snd_route = time_at_stop_in(schedule, stops[i], routes[i]);
        if (!ith_stop_fst_route.has_value() || !ith_stop_snd_route.has_value())
            return std::nullopt;
        // check if you have to wait at the current stop
        int time_difference = minutes_between(ith_stop_fst_route.value(), ith_stop_snd_route.value());

        if (time_difference > 1)
            return stops[i];
    }
    return std::nullopt;
}

static Answer find_best_route(const Question &question, Tickets &tickets, Schedule &schedule)
{
    // name of the stop where you have to wait - empty if you don't have to wait
    auto name_of_waiting_stop_opt = get_waiting_stop_name(schedule, question);

    if(name_of_waiting_stop_opt.has_value())
        return TICKET_WAIT_ANSWER_PREFIX + name_of_waiting_stop_opt.value();

    auto stops = question.first;
    auto routes = question.second;

    auto start_time = time_at_stop_in(schedule, stops.front(), routes.front());
    auto end_time = time_at_stop_in(schedule, stops.back(), routes.back());
    if (!start_time.has_value() || !end_time.has_value())
        return TICKET_NEGATIVE_ANSWER;

    int total_minutes = minutes_between(start_time.value(), end_time.value());

    Price minimum_price = std::make_pair(INF, 0);
    int ticket_count = 0;
    TicketsAnswer optimal_tickets_names(3);

    for (auto const &i: tickets)
        for (auto const &j: tickets)
            for (auto const &k: tickets) {
                std::array<TicketsEntry, MAX_TICKETS> curr_tickets = {i, j, k};

                Price curr_price = MIN_PRICE;
                int curr_minutes = 0;

                for (int x = 0; x < MAX_TICKETS; ++x) {
                    add_ticket_cost(curr_price, curr_minutes, curr_tickets[x].second);

                    if (curr_minutes >= total_minutes && curr_price < minimum_price) {
                        minimum_price = curr_price;
                        ticket_count = x + 1;
                        for(int y = 0; y < ticket_count; ++y)
                            optimal_tickets_names[y] = curr_tickets[y].first;
                    }
                }
            }

    // if optimal ticket choice not found
    if(ticket_count == 0) {
        Answer answer = TICKET_NEGATIVE_ANSWER;
        return answer;
    }
    TicketsAnswer final_answer(optimal_tickets_names.begin(),
                               optimal_tickets_names.begin() + ticket_count);
    return final_answer;
}

static void print_answer(const Answer &answer)
{
    if (std::holds_alternative<std::string>(answer)) {
        std::cout << std::get<std::string>(answer) << std::endl;
    } else {
        std::cout << TICKET_POSITIVE_ANSWER_PREFIX;
        auto const &tickets = std::get<TicketsAnswer>(answer);
        for (size_t i = 0; i < tickets.size() - 1; ++i)
            std::cout << tickets[i] << TICKETS_ANSWER_SEPARATOR;
        std::cout << tickets.back() << std::endl;
    }
}

static void print_error(const std::string &input_line, const size_t &line_number)
{
    std::cerr << ERROR_LINE_PREFIX << line_number << ERROR_LINE_SEPARATOR
              << input_line << std::endl;
}