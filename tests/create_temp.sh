#!/bin/bash
# Author: Maciej Procyk

for f in $1/*.out
do
	cp ${f} ${f%out}tempout
done

for f in $1/*.err
do
	cp ${f} ${f%err}temperr
done
